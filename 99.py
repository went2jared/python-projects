bottleCount = 99

while (bottleCount > 1):
	print bottleCount, 'Bottles of beer on the wall', bottleCount, 'bottles of beer'
	bottleCount -= 1
	print 'Take one down pass it around', bottleCount, 'bottles of beer on the wall \n'
else:
	print bottleCount, 'Bottle of beer on the wall', bottleCount, 'bottle of beer'
	bottleCount -= 1
	print 'Take one down pass it around no more bottles of beer on the wall \n'
	print 'No more bottles of beer on the wall, no more bottles of beer'
	bottleCount += 99
	print 'Go to the store and buy some more', bottleCount, 'bottles of beer on the wall'
