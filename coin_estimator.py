#this program in designed to estimate the amount of coin rolls
#are needed based off of the total weight of each coin type
#the user in put weight of each type of coin
#the program will estimate how many of each coin type there are
#then calculate how many coin rolls that it will fill
#it will print how many coin rolls are needed, count of each coin type
#and print total estimated value
import math
import itertools
import sys
#User input for weight of coins
type = ['Penny', 'Nickel', 'Dime', 'Quarter']
def userInput():
	inputList = []
	for i in type:
		while True:
			weight = input('Please enter total ' + i + ' weight:')
			if inputValidator(i, weight) == False:
				inputList.append(weight)
				False
				break
		pass
	return inputList
#validation for user input
def inputValidator(type, weight):
	try:
		weight = float(weight)
	except ValueError:
		print('Please enter valid number')
		return True
	else:
		if weight >= 0:
			print('Weight of', type, ': ', weight)
			return False
		else:
			print ('Please enter a positive number')
			return True
	pass
#calculates the count for each type of coin
def coinCount(weightList):
	pennyCount = float(weightList[0])/2.5
	nickelCount = float(weightList[1])/5.0
	dimeCount = float(weightList[2])/2.268
	quarterCount = float(weightList[3])/5.670
	countList = [pennyCount, nickelCount, dimeCount, quarterCount]
	return countList
#calculates number of rolls needed
def totalRoll(countList):
	pennyRollTotal = math.floor(float(countList[0])/50)
	nickelRollTotal = math.floor(float(countList[1])/40)
	dimeRollTotal = math.floor(float(countList[2])/50)
	quarterRollTotal = math.floor(float(countList[3])/40)
	rollTotal = [pennyRollTotal, nickelRollTotal, dimeRollTotal, quarterRollTotal]
	return rollTotal

#calculates total value for each type of coin
def totalCoin(countList):
	pennyTotal = float(countList[0])*.01
	nickelTotal = float(countList[1])*.05
	dimeTotal = float(countList[2])*.10
	quarterTotal = float(countList[3])*.25
	coinTotal = [pennyTotal, nickelTotal, dimeTotal, quarterTotal]
	return coinTotal

#print results
def printResults(type, totalRoll, countList, coinTotal):
	#print total rolls per type
	for n, r in itertools.zip_longest(type, totalRoll):
		print(n, 'rolls needed:', int(r))
	#print count of each coin type
	for n, ct in itertools.zip_longest(type,countList):
		print(n, 'count:', int(ct))
	#print total est. value of coins
	print('Total estimated value:', '${:,.2f}'.format(sum(coinTotal)))
	
#check if user wants to run program again
def newInput():
	#user input to test again with multiple acceptable inputs
	while True:
		again = input('Would you like to count again?').lower()
		if any ([again == "yes", again == "y"]):
			False
			return 'Yes'
		elif any ([again == "no", again == "n"]):
			print ('Goodbye!')
			False
			return 'No'
		else:
			print ('Invalid input')
			True
			pass

while True:
	weightList = userInput()
	countList = coinCount(weightList)
	coinTotal = totalCoin(countList)
	rollTotal = totalRoll(countList)
	printResults(type, rollTotal, countList, coinTotal)
	if newInput() == 'Yes':
		True
	else:
		sys.exit(0)
	pass
