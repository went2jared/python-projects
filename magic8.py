import random
import time
import sys

response = ['Yes, most definitely!', 'The chances are high!', 'Not likely', 'May the odds be ever in your favor', 'Not a chance in the world', 'Try it out and see!', 'Ask again later', 'Cannot predict now', "Don't count on it", 'IDK']
def userInput():
	question = input('Enter your question: ')
	print (question)
	print ('thinking...')
	time.sleep(3)
	print (random.choice(response))

	final()

def final():
	play = input('Would you like to play again: ').lower()
	if any ([play == "y", play == "yes"]):
		userInput()
	elif any ([play == "n", play == "no"]):
		print ('Good bye!')
		sys.exit(0)
	else:
		print('Invalid input')
		final()

print ('Welcome to the Magic 8 ball!')
userInput()
