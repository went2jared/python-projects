import sys

def userInput():
	# User input validation
	while True:
		try:
			side1 = int(input('Size of side 1: '))
		except ValueError:
			print("Please enter a valid number")
		else: 
			if side1 >= 1:
				print (side1)
				False
				break
			else:
				print ("Please enter a positive number")
				True
		pass
	while True:
		try:
			side2 = int(input('Size of side 2: '))
		except ValueError:
			print("Please enter a valid number")
		else:
			if side2 >=1:
				print (side2)
				False
				break
			else:
				print ("Please enter a positive number")
				True
		pass
	while True:
		try:
			side3 = int(input('Size of side 3: '))
		except ValueError:
			print("Please enter a valid number")
		else:
			if side3 >=1:
				print (side3)
				False
				break
			else:
				print ("Please enter a positive number")
				True
		pass
	#call calculate function to calculate it numbers are pythagorean triple
	calculate(side1, side2, side3)

def calculate(side1, side2, side3):
	#insert user input values into list
	#sort from smallest to largest and assign to variables
	sides = [side1, side2, side3]
	sides.sort()
	powSide1 = sides[0]**2
	powSide2 = sides[1]**2
	powSide3 = sides[2]**2
	#calculate if values are pythagorean triple
	if (powSide1 + powSide2 == powSide3):
		print ('The triangle is a pythagorean triple')
	else:
		print ('The triangle is NOT a pythagorean triple')
	#call new input function to ask if user wants to check another triangle
	newInput()

def newInput():
	#user input to test again with multiple acceptable inputs
	again = input('Would you like to test another triangle?').lower()
	if any ([again == "yes", again == "y"]):
		userInput()
	elif any ([again == "no", again == "n"]):
		print ('Goodbye!')
		sys.exit(0)
	else:
		print ('Invalid input')
		newInput()
#start program
print ('Welcome!')
userInput()